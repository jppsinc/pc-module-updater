﻿namespace Customs
{
    partial class MainForm
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.UpdateButton = new System.Windows.Forms.Button();
            this.CloseButton = new System.Windows.Forms.Button();
            this.MessageOnTop = new System.Windows.Forms.Label();
            this.UpdateDesc = new System.Windows.Forms.TextBox();
            this.UpdateDescLabel = new System.Windows.Forms.Label();
            this.FileList = new System.Windows.Forms.DataGridView();
            this.FileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LocalVersion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ServerVersion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DoUpdate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.FileList)).BeginInit();
            this.SuspendLayout();
            // 
            // UpdateButton
            // 
            this.UpdateButton.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.UpdateButton.Location = new System.Drawing.Point(306, 399);
            this.UpdateButton.Name = "UpdateButton";
            this.UpdateButton.Size = new System.Drawing.Size(150, 30);
            this.UpdateButton.TabIndex = 0;
            this.UpdateButton.Text = "アップデートを進む";
            this.UpdateButton.UseVisualStyleBackColor = true;
            this.UpdateButton.Click += new System.EventHandler(this.UpdateButton_Click);
            // 
            // CloseButton
            // 
            this.CloseButton.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.CloseButton.Location = new System.Drawing.Point(462, 399);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(150, 30);
            this.CloseButton.TabIndex = 1;
            this.CloseButton.Text = "閉じる";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // MessageOnTop
            // 
            this.MessageOnTop.AutoSize = true;
            this.MessageOnTop.Font = new System.Drawing.Font("Meiryo UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.MessageOnTop.ForeColor = System.Drawing.Color.Red;
            this.MessageOnTop.Location = new System.Drawing.Point(12, 9);
            this.MessageOnTop.Name = "MessageOnTop";
            this.MessageOnTop.Size = new System.Drawing.Size(82, 24);
            this.MessageOnTop.TabIndex = 3;
            this.MessageOnTop.Text = "メッセージ";
            // 
            // UpdateDesc
            // 
            this.UpdateDesc.BackColor = System.Drawing.SystemColors.Window;
            this.UpdateDesc.Font = new System.Drawing.Font("メイリオ", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UpdateDesc.Location = new System.Drawing.Point(12, 59);
            this.UpdateDesc.Multiline = true;
            this.UpdateDesc.Name = "UpdateDesc";
            this.UpdateDesc.ReadOnly = true;
            this.UpdateDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.UpdateDesc.Size = new System.Drawing.Size(600, 80);
            this.UpdateDesc.TabIndex = 4;
            // 
            // UpdateDescLabel
            // 
            this.UpdateDescLabel.AutoSize = true;
            this.UpdateDescLabel.Font = new System.Drawing.Font("メイリオ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.UpdateDescLabel.Location = new System.Drawing.Point(13, 38);
            this.UpdateDescLabel.Name = "UpdateDescLabel";
            this.UpdateDescLabel.Size = new System.Drawing.Size(104, 18);
            this.UpdateDescLabel.TabIndex = 5;
            this.UpdateDescLabel.Text = "アップデート情報";
            // 
            // FileList
            // 
            this.FileList.AllowUserToAddRows = false;
            this.FileList.AllowUserToDeleteRows = false;
            this.FileList.BackgroundColor = System.Drawing.SystemColors.Control;
            this.FileList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.FileList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.FileList.ColumnHeadersVisible = false;
            this.FileList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FileName,
            this.LocalVersion,
            this.ServerVersion,
            this.DoUpdate});
            this.FileList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.FileList.Location = new System.Drawing.Point(12, 145);
            this.FileList.Name = "FileList";
            this.FileList.ReadOnly = true;
            this.FileList.RowHeadersVisible = false;
            this.FileList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.FileList.RowTemplate.Height = 21;
            this.FileList.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.FileList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.FileList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.FileList.Size = new System.Drawing.Size(600, 248);
            this.FileList.TabIndex = 7;
            // 
            // FileName
            // 
            dataGridViewCellStyle1.Font = new System.Drawing.Font("メイリオ", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FileName.DefaultCellStyle = dataGridViewCellStyle1;
            this.FileName.HeaderText = "FileName";
            this.FileName.MinimumWidth = 10;
            this.FileName.Name = "FileName";
            this.FileName.ReadOnly = true;
            this.FileName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.FileName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FileName.Width = 185;
            // 
            // LocalVersion
            // 
            dataGridViewCellStyle2.Font = new System.Drawing.Font("メイリオ", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LocalVersion.DefaultCellStyle = dataGridViewCellStyle2;
            this.LocalVersion.HeaderText = "LocalVersion";
            this.LocalVersion.MinimumWidth = 10;
            this.LocalVersion.Name = "LocalVersion";
            this.LocalVersion.ReadOnly = true;
            this.LocalVersion.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.LocalVersion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.LocalVersion.Width = 175;
            // 
            // ServerVersion
            // 
            dataGridViewCellStyle3.Font = new System.Drawing.Font("メイリオ", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ServerVersion.DefaultCellStyle = dataGridViewCellStyle3;
            this.ServerVersion.HeaderText = "ServerVersion";
            this.ServerVersion.MinimumWidth = 10;
            this.ServerVersion.Name = "ServerVersion";
            this.ServerVersion.ReadOnly = true;
            this.ServerVersion.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ServerVersion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ServerVersion.Width = 175;
            // 
            // DoUpdate
            // 
            dataGridViewCellStyle4.Font = new System.Drawing.Font("メイリオ", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.DoUpdate.DefaultCellStyle = dataGridViewCellStyle4;
            this.DoUpdate.HeaderText = "DoUpdate";
            this.DoUpdate.MinimumWidth = 10;
            this.DoUpdate.Name = "DoUpdate";
            this.DoUpdate.ReadOnly = true;
            this.DoUpdate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.DoUpdate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.DoUpdate.Width = 40;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 441);
            this.Controls.Add(this.FileList);
            this.Controls.Add(this.UpdateDescLabel);
            this.Controls.Add(this.UpdateDesc);
            this.Controls.Add(this.MessageOnTop);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.UpdateButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "自動アップデータ";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.FileList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button UpdateButton;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Label MessageOnTop;
        private System.Windows.Forms.TextBox UpdateDesc;
        private System.Windows.Forms.Label UpdateDescLabel;
        private System.Windows.Forms.DataGridView FileList;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn LocalVersion;
        private System.Windows.Forms.DataGridViewTextBoxColumn ServerVersion;
        private System.Windows.Forms.DataGridViewTextBoxColumn DoUpdate;
    }
}

