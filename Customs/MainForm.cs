﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace Customs
{
    public partial class MainForm : Form
    {
        private Modes _modes;

        private string _exeFile = string.Empty;


        public MainForm(bool doUpdate, Modes modeDef)
        {
            InitializeComponent();

            UpdateButton.Enabled = doUpdate;

            if (doUpdate)
            {
                MessageOnTop.Text = "新しいアップデートがあります。";
            }
            else
            {
                MessageOnTop.Text = "最新のバージョンです。";
            }

            _modes = new Modes();
            _modes = modeDef;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (_modes.IsVersion4)
            {
                if (_modes.IsAreaModule)
                {
                    _exeFile = FixedString.ExecutionFile4Area;
                }
                else
                {
                    _exeFile = FixedString.ExecutionFile4;
                }
            }
            else
            {
                _exeFile = FixedString.ExecutionFile3;
            }

            if (_modes.Quiet)
            {
                UpdateButton_Click(null, null);
            }
            else
            {
                Text = Text + " / VERSION: " + Application.ProductVersion;

                foreach (var item in Processor.Instance.TargetFileList)
                {
                    string doUpdate = string.Empty;

                    if (item.DoUpdate)
                    {
                        doUpdate = "O";
                    }

                    FileList.Rows.Add(item.FileName, item.LocalVersion, item.ServerVersion, doUpdate);
                }

                string flg = string.Empty;

                if (_modes.IsVersion4)
                {
                    if (_modes.IsAreaModule)
                    {
                        flg = FixedString.CommandLatestNotice4Area;
                    }
                    else
                    {
                        flg = FixedString.CommandLatestNotice4;
                    }
                }
                else
                {
                    flg = FixedString.CommandLatestNotice3;
                }

                string res;
                if (Processor.Instance.GetResponse(flg, out res, _modes.Dev, _modes.IsVersion4))
                {
                    if (!string.IsNullOrEmpty(res))
                    {
                        int cnt = 0;
                        string[] tmp = res.Split('$');

                        for (int i = 0; i < tmp.Length; i++)
                        {
                            if (string.IsNullOrEmpty(tmp[i]))
                            {
                                continue;
                            }
                            else
                            {
                                if (_modes.IsVersion4)
                                {
                                    if (cnt == 0)
                                    {
                                        UpdateDesc.Text += string.Format("< {0} >", tmp[i].Trim());
                                        cnt++;

                                        continue;
                                    }
                                    else
                                    {
                                        UpdateDesc.Text += Environment.NewLine;
                                    }
                                    UpdateDesc.Text += string.Format("{0}. {1}", cnt, tmp[i].Trim());
                                    cnt++;
                                }
                                else
                                {
                                    if (i != 0)
                                    {
                                        UpdateDesc.Text += Environment.NewLine;
                                    }
                                    UpdateDesc.Text += string.Format("{0}. {1}", i + 1, tmp[i].Trim());
                                }
                            }
                        }
                    }
                }
            }
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            bool doSelfUpdate = false;

            if (!_modes.Test)
            {
                string tmpPath = Environment.CurrentDirectory + "\\tmp";

                Directory.CreateDirectory(tmpPath);

                foreach (var item in Processor.Instance.TargetFileList)
                {
                    if (item.DoUpdate)
                    {
                        try
                        {
                            if (item.LocalVersion.Equals(FixedString.Copy))
                            {
                                if (!ReplaceFile(tmpPath, item.FileName))
                                {
                                    continue;
                                }
                            }
                            else if (item.ServerVersion.Equals(FixedString.Delete))
                            {
                                File.Delete(item.FileName);
                            }
                            else if (item.FileName.Equals("updater.exe"))
                            {
                                doSelfUpdate = true;
                            }
                            else
                            {
                                if (!ReplaceFile(tmpPath, item.FileName))
                                {
                                    continue;
                                }
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }

                Directory.Delete(tmpPath, true);

                MessageBox.Show("アップデートが終了しました。モジュールを再起動します。", "通知", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            if (doSelfUpdate)
            {
                ReplaceSelf();
            }
            else
            {
                if (File.Exists(Environment.CurrentDirectory + "\\" + _exeFile))
                {
                    string args = string.Empty;

                    if (_modes.Dev)
                    {
                        if (!string.IsNullOrEmpty(args))
                        {
                            args += " ";
                        }
                        args += "-dev";
                    }

                    if (_modes.Test)
                    {
                        if (!string.IsNullOrEmpty(args))
                        {
                            args += " ";
                        }
                        args += "-noupdate";
                    }

                    if (string.IsNullOrEmpty(args))
                    {
                        Process.Start(_exeFile);
                    }
                    else
                    {
                        Process.Start(_exeFile, args);
                    }
                }

                Close();
            }
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private bool ReplaceFile(string path, string filename)
        {
            bool result = true;

            string server = string.Format("{0}/{1}", Processor.Instance.GetServerUrl(_modes.Dev, _modes.IsVersion4, _modes.IsAreaModule), filename);
            string local = string.Format("{0}\\{1}", path, filename);

            try
            {
                ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

                using (WebClient wc = new WebClient())
                {
                    wc.Credentials = new NetworkCredential("psinc", "psinc");
                    wc.DownloadFile(server, local);
                }

                // Target file.
                string target = Environment.CurrentDirectory + "\\" + filename;

                // Delete an old-version file.
                if (File.Exists(target))
                {
                    File.Delete(target);
                }

                // Copy a new-version file.
                File.Copy(path + "\\" + filename, target);
            }
            catch
            {
                result = false;
            }

            return result;
        }

        private void ReplaceSelf()
        {
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

            using (WebClient wc = new WebClient())
            {
                wc.Credentials = new NetworkCredential("psinc", "psinc");
                wc.DownloadFile(string.Format("{0}/{1}", Processor.Instance.GetServerUrl(_modes.Dev, _modes.IsVersion4, _modes.IsAreaModule), "updater.exe"), Environment.CurrentDirectory + "\\updater_tmp.exe");
            }

            using (var batFile = new StreamWriter(File.Create("update.bat")))
            {
                batFile.WriteLine("@ECHO OFF");
                batFile.WriteLine("TIMEOUT /t 1 /nobreak > NUL");
                batFile.WriteLine("TASKKILL /IM \"updater.exe\" > NUL");
                batFile.WriteLine("MOVE \"updater_tmp.exe\" \"updater.exe\"");
                batFile.WriteLine("DEL \"%~f0\" & START \"\" /B \"{0}\"", _exeFile);
            }

            ProcessStartInfo psi = new ProcessStartInfo("update.bat");
            psi.UseShellExecute = false;
            psi.CreateNoWindow = true;
            psi.WorkingDirectory = Environment.CurrentDirectory;

            Process.Start(psi);

            Environment.Exit(0);
        }
    }
}