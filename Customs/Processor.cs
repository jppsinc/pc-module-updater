﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace Customs
{
    public struct Modes
    {
        public bool Quiet;

        public bool Dev;

        public bool Test;

        public bool IsVersion4;

        public bool IsAreaModule;
    }

    public struct TargetFileInfo
    {
        public string FileName;

        public string LocalVersion;

        public string ServerVersion;

        public bool DoUpdate;
    }

    public class Processor
    {
        private static Processor _instance = null;

        private List<TargetFileInfo> _targetFileList;


        private Processor()
        {
            _targetFileList = new List<TargetFileInfo>();
        }

        public static Processor Instance
        {
            get
            {
                _instance = _instance ?? new Processor();
                return _instance;
            }
        }

        public List<TargetFileInfo> TargetFileList
        {
            get
            {
                return _targetFileList;
            }
        }

        public bool UpdateNeeded(bool isDevMode, bool isVersion4, bool isAreaModule)
        {
            bool result = false;

            // List of files from server.
            List<string> serverFiles = new List<string>();

            string flg = string.Empty;
            if (isVersion4)
            {
                if (isAreaModule)
                {
                    flg = FixedString.CommandLatestFiles4Area;
                }
                else
                {
                    flg = FixedString.CommandLatestFiles4;
                }
            }
            else
            {
                flg = FixedString.CommandLatestFiles3;
            }

            string res;
            if (GetResponse(flg, out res, isDevMode, isVersion4))
            {
                if (string.IsNullOrEmpty(res))
                {
                    System.Windows.Forms.MessageBox.Show("サーバーに接続できません。次の実行時、もう一度確認します。", "エラー");
                    return result;
                }
                else
                {
                    foreach (var item in res.Split('#'))
                    {
                        serverFiles.Add(item);
                    }
                }
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("サーバーに接続できません。次の実行時、もう一度確認します。", "エラー");
                return result;
            }

            // List of files from local.
            DirectoryInfo source = new DirectoryInfo(Environment.CurrentDirectory);
            string[] extensions = new[] { ".exe", ".dll" };

            FileInfo[] localFiles = source.GetFiles().Where(f => extensions.Contains(f.Extension.ToLower())).ToArray();

            if (localFiles.Length > 0)
            {
                foreach (var item in localFiles)
                {
                    // EXCEPTION
                    if (item.Name.Equals("uninst.exe"))
                    {
                        continue;
                    }
                    else if (IsHostingProcess(item))
                    {
                        continue;
                    }

                    // COMPARISON
                    FileVersionInfo vi = null;
                    vi = FileVersionInfo.GetVersionInfo(item.FullName);

                    TargetFileInfo tf = new TargetFileInfo();
                    tf.FileName = item.Name;
                    tf.LocalVersion = vi.ProductVersion;

                    for (int i = 0; i < serverFiles.Count; i++)
                    {
                        if (item.Name.Equals(serverFiles[i].Split('=')[0]))
                        {
                            tf.ServerVersion = serverFiles[i].Split('=')[1];
                            serverFiles.RemoveAt(i);
                            break;
                        }
                    }

                    if (tf.ServerVersion == null)
                    {
                        tf.ServerVersion = FixedString.Delete;
                    }

                    tf.DoUpdate = !tf.ServerVersion.Equals(tf.LocalVersion);

                    _targetFileList.Add(tf);
                }
            }

            if (serverFiles.Count > 0)
            {
                foreach (var item in serverFiles)
                {
                    TargetFileInfo tf = new TargetFileInfo();

                    tf.FileName = item.Split('=')[0];
                    tf.LocalVersion = FixedString.Copy;
                    tf.ServerVersion = item.Split('=')[1];
                    tf.DoUpdate = true;

                    _targetFileList.Add(tf);
                }
            }

            foreach (var item in _targetFileList)
            {
                if (item.DoUpdate)
                {
                    result = true;
                    break;
                }
            }

            return result;
        }

        public string GetServerUrl(bool isDevMode, bool isVersion4, bool isAreaModule)
        {
            if (isDevMode)
            {
                if (isVersion4)
                {
                    if (isAreaModule)
                    {
                        return FixedString.DevServer4Area;
                    }
                    else
                    {
                        return FixedString.DevServer4;
                    }
                }
                else
                {
                    return FixedString.DevServer3;
                }
            }
            else
            {
                if (isVersion4)
                {
                    if (isAreaModule)
                    {
                        return FixedString.PrdServer4Area;
                    }
                    else
                    {
                        return FixedString.PrdServer4;
                    }
                }
                else
                {
                    return FixedString.PrdServer3;
                }
            }
        }

        public bool GetResponse(string parameters, out string response, bool isDevMode, bool isVersion4)
        {
            bool result = true;
            string url = string.Empty;

            if (isDevMode)
            {
                if (isVersion4)
                {
                    url = FixedString.DevUrl4;
                }
                else
                {
                    url = FixedString.DevUrl3;
                }
            }
            else
            {
                if (isVersion4)
                {
                    url = FixedString.PrdUrl4;
                }
                else
                {
                    url = FixedString.PrdUrl3;
                }
            }

            response = string.Empty;

            try
            {
                HttpWebRequest webReq = null;

                if (isVersion4)
                {
                    webReq = (HttpWebRequest)WebRequest.Create(string.Format(url, parameters));
                    webReq.Method = WebRequestMethods.Http.Get;
                    webReq.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
                }
                else
                {
                    byte[] paramsByte = Encoding.UTF8.GetBytes(parameters);

                    webReq = (HttpWebRequest)WebRequest.Create(url);
                    webReq.Method = WebRequestMethods.Http.Post;
                    webReq.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
                    webReq.ContentLength = paramsByte.Length;

                    Stream requestStream = webReq.GetRequestStream();
                    requestStream.Write(paramsByte, 0, paramsByte.Length);
                    requestStream.Close();
                }

                HttpWebResponse webRes = (HttpWebResponse)webReq.GetResponse();

                using (var responseStream = webRes.GetResponseStream())
                {
                    using (var sr = new StreamReader(responseStream, Encoding.UTF8))
                    {
                        response = sr.ReadToEnd();
                    }
                }
                webRes.Close();
            }
            catch
            {
                result = false;
            }

            return result;
        }

        private bool IsHostingProcess(FileInfo target)
        {
            bool result = false;

            if (target.Extension.Equals(".exe"))
            {
                var vi = FileVersionInfo.GetVersionInfo(target.FullName);

                string[] filename = target.Name.Split('.');

                if (filename.Length > 2 && vi.FileDescription.Equals("vshost32.exe") && filename[filename.Length - 2].Equals("vshost"))
                {
                    result = true;
                }
            }

            return result;
        }
    }
}