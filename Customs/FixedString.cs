﻿namespace Customs
{
    internal class FixedString
    {   // Created: 2018.01.17

        internal static readonly string Copy = "COPY";

        internal static readonly string Delete = "DELETE";

        /* RepChecker PC Module 3.x */
        internal static readonly string PrdServer3 = "https://repchecker.jp/login/management/pricecheck/download/new_module_file_list";

        internal static readonly string DevServer3 = "https://stage.repchecker.jp/login/management/pricecheck/download/new_module_file_list";

        internal static readonly string CommandLatestFiles3 = "flg=get_version";

        internal static readonly string CommandLatestNotice3 = "flg=get_latest_notice";

        internal static readonly string PrdUrl3 = "https://repchecker.jp/getinfo/chk_login.php";

        internal static readonly string DevUrl3 = "https://stage.repchecker.jp/getinfo/chk_login.php";

        internal static readonly string ExecutionFile3 = "kakaku.exe";

        /* RepChecker PC Module 4.x */
        internal static readonly string PrdServer4 = "https://admin.repchecker.jp/module";

        internal static readonly string DevServer4 = "https://dev.admin.repchecker.jp/module";

        internal static readonly string CommandLatestFiles4 = "latestFileInfo";

        internal static readonly string CommandLatestNotice4 = "latestUpdateInfo";

        internal static readonly string PrdUrl4 = "https://moduleapi.repchecker.jp/api/{0}";

        internal static readonly string DevUrl4 = "http://localhost:8000/api/{0}";

        internal static readonly string ExecutionFile4 = "rcpcmodule.exe";

        /* RepChecker PC Module (Area) 4.x */
        internal static readonly string PrdServer4Area = "https://admin.repchecker.jp/module-area";

        internal static readonly string DevServer4Area = "https://dev.admin.repchecker.jp/module-area";

        internal static readonly string CommandLatestFiles4Area = "area/latest-file-info";

        internal static readonly string CommandLatestNotice4Area = "area/latest-update-info";

        internal static readonly string ExecutionFile4Area = "rcpcm_area.exe";
    }
}