﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Customs
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            string[] args = Environment.GetCommandLineArgs();

            bool updateNeeded = false;
            bool loadMainForm = true;

            Modes mod = new Modes();

            if (args.Length > 1)
            {
                for (int i = 0; i < args.Length; i++)
                {
                    if (args[i].Equals("-4"))
                    {
                        mod.IsVersion4 = true;
                        continue;
                    }

                    if (args[i].Equals("-area"))
                    {
                        mod.IsAreaModule = true;
                        continue;
                    }

                    if (args[i].Equals("-checkupdate")) // Only writes a result on file. Ignores options below.
                    {
                        updateNeeded = Processor.Instance.UpdateNeeded(false, mod.IsVersion4, mod.IsAreaModule); // Can't use a dev server when "checkupdate" option.

                        using (var sw = new StreamWriter("update_info", false, Encoding.UTF8))
                        {
                            sw.WriteLine(updateNeeded.ToString().ToLower());
                        }

                        loadMainForm = false;

                        continue;
                    }

                    if (args[i].Equals("-quiet"))
                    {
                        mod.Quiet = true;
                        continue;
                    }

                    if (args[i].Equals("-dev"))
                    {
                        mod.Dev = true;
                        continue;
                    }

                    if (args[i].Equals("-test"))
                    {
                        mod.Test = true;
                        continue;
                    }
                }
            }
            
            if (loadMainForm)
            {
                updateNeeded = Processor.Instance.UpdateNeeded(mod.Dev, mod.IsVersion4, mod.IsAreaModule);

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new MainForm(updateNeeded, mod));
            }
        }
    }
}